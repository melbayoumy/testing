import React, { FC, useState } from "react";
import User from "./components/User/User";
import Start from "./components/Start/Start";

// styles
import classes from "./App.module.scss";
import { Result } from "./lib/server/calls/types";
import Question from "./components/Question/Question";

const App: FC = () => {
  const [email, setEmail] = useState<string>("");
  const [result, setResult] = useState<Result | null>(null);
  const [displayQuestion, setDisplayQuestion] = useState(false);

  let content = (
    <Start
      email={email}
      result={result}
      setDisplayQuestion={setDisplayQuestion}
    />
  );

  if (!email || !result) {
    content = <User setEmail={setEmail} setResult={setResult} />;
  }

  if (displayQuestion && result) {
    content = <Question result={result} />;
  }

  return <div className={classes.App}>{content}</div>;
};

export default App;
