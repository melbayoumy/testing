import { AxiosResponse } from "axios"
import { sqlClient } from "../../clients"
import { Result } from "../types"

export const startAnswering = (resultId: number): Promise<AxiosResponse<Result>> => {
    return sqlClient.patch(`/v1/results/${resultId}/start`);
}

export const checkAnswer = (questionId: number, answer: string): Promise<AxiosResponse<{ correct: boolean; msg: string }>> => {
    return sqlClient.post(`/v1/results/question/${questionId}/check`, JSON.stringify(answer));
}

export const submitAnswer = (resultId: number, answer: string): Promise<AxiosResponse<Result>> => {
    return sqlClient.post(`/v1/results/${resultId}`, JSON.stringify(answer));
}
