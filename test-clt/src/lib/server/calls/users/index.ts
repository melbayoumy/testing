import { AxiosResponse } from "axios"
import { usersClient } from "../../clients"
import { Result } from "../types"

export const getCurrentResult = (email: string): Promise<AxiosResponse<Result>> => {
    return usersClient.get(`/v1/results/email/${email}`);
}