export interface Result {
    id: number;
    question: Question;
    userId: number;
    startedOn: string;
    finishedOn: string | null;
    isCorrect: string | null;
    queueNo: number;
}

export interface Question {
    id: number;
    score: number;
    text: string;
}
