import axios from 'axios';

export const usersClient = axios.create({
    baseURL: process.env.REACT_APP_URL_SRV_USERS,
    headers: {
        'Content-Type': 'application/json'
    }
});

export const sqlClient = axios.create({
    baseURL: process.env.REACT_APP_URL_SRV_SQL,
    headers: {
        'Content-Type': 'application/json'
    }
});
