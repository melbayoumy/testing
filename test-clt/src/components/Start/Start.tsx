import { AxiosError } from "axios";
import React, { FC, useCallback, useState } from "react";
import { startAnswering } from "../../lib/server/calls/sql";
import { Result } from "../../lib/server/calls/types";
import Alert, { AlertType } from "../Alert/Alert";

interface Props {
  email: string;
  result: Result | null;
  setDisplayQuestion: (_: boolean) => void;
}

const Start: FC<Props> = ({ email, result, setDisplayQuestion }) => {
  const [error, setError] = useState<string | null>(null);
  const [loading, setLoading] = useState(false);

  const handleClick = useCallback(() => {
    setError(null);

    if (!result) {
      return;
    }

    setLoading(true);
    const error = "Server error! please try again later";

    startAnswering(result.id)
      .then(({ data }) => {
        if (!data) {
          setError(error);
          setLoading(false);
          return;
        }

        setDisplayQuestion(true);
      })
      .catch((e: AxiosError) => {
        if (e.response?.status == 410) {
          setDisplayQuestion(true);

          return;
        }

        setLoading(false);
        setError(error);
      });
  }, [result, setDisplayQuestion]);

  return (
    <>
      {!!error && <Alert type={AlertType.DANGER}>{error}</Alert>}
      <p>
        Thanks <strong>{email}</strong>! <br />
        You can proceed with your tests now...
      </p>
      <button
        type="button"
        className="btn"
        disabled={loading}
        onClick={handleClick}
      >
        Start
      </button>
    </>
  );
};

export default Start;
