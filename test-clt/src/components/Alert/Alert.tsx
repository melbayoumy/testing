import React, { FC, ReactNode } from "react";

// styles
import classes from "./Alert.module.scss";

export enum AlertType {
  SUCCESS = "Alert--success",
  DANGER = "Alert--danger",
}

interface Props {
  children: string | ReactNode;
  type: AlertType;
}

const Alert: FC<Props> = ({ children, type }) => {
  return <div className={`${classes.Alert} ${type}`}>{children}</div>;
};

export default Alert;
