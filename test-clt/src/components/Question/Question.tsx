import { ErrorMessage, Field, Form, Formik, FormikHelpers } from "formik";
import React, { FC, useCallback } from "react";
import { Result } from "../../lib/server/calls/types";
import * as Yup from "yup";
import { checkAnswer, submitAnswer } from "../../lib/server/calls/sql";
import { AxiosError } from "axios";
import Alert, { AlertType } from "../Alert/Alert";

// styles
import classes from "./Question.module.scss";

interface Props {
  result: Result;
}

interface Values {
  answer: string;
}

const Question: FC<Props> = ({ result }) => {
  const validationSchema = Yup.object().shape({
    answer: Yup.string().required("Please insert an answer"),
  });

  const handleExecute = useCallback(
    async (values: Values, { setSubmitting, setStatus, validateForm }) => {
      setStatus({});

      const v = await validateForm();

      if (Object.keys(v).length) {
        return;
      }

      setSubmitting(true);

      const serverError = "Server error! please try again later";

      checkAnswer(result?.question.id, values.answer)
        .then(({ data }) => {
          if (!data) {
            setStatus({ serverError });
            return;
          }

          setStatus({
            error: !data.correct,
            msg: data.correct ? "Correct answer" : data.msg || "Wrong Answer",
          });
        })
        .catch(() => {
          setStatus({ serverError });
        })
        .finally(() => setSubmitting(false));
    },
    [result?.question.id]
  );

  const handleSubmit = useCallback(
    (values: Values, { setSubmitting, setStatus }: FormikHelpers<Values>) => {
      if (!confirm("Are you sure you want to submit your answer?")) {
        setSubmitting(false);
        return;
      }

      setSubmitting(true);
      setStatus({});

      const serverError = "Server error! please try again later";

      submitAnswer(result?.id, values.answer)
        .then((resp) => {
          if (![200, 204].includes(resp.status)) {
            setStatus({ serverError });
            return;
          }

          setStatus({
            error: false,
            msg: "Your answer was submitted successfully",
          });
        })
        .catch(() => {
          setStatus({ serverError });
        })
        .finally(() => setSubmitting(false));
    },
    [result?.id]
  );

  return (
    <div className={classes.Question}>
      <header className={classes.Header}>
        <div>
          <strong>Started on: </strong>
          {new Date(result.startedOn).toLocaleDateString()}{" "}
          {new Date(result.startedOn).toLocaleTimeString()}
        </div>
        <div>
          <strong>Score: </strong>
          {result.question.score}
        </div>
      </header>

      <div dangerouslySetInnerHTML={{ __html: result.question.text }} />

      <Formik
        initialValues={{
          answer: "",
        }}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({
          isSubmitting,
          status,
          values,
          setSubmitting,
          setStatus,
          validateForm,
          setErrors,
        }) => {
          return (
            <Form>
              <div className="form-group">
                <Field
                  as="textarea"
                  name="answer"
                  placeholder="your answer"
                  className="form-field"
                  disabled={isSubmitting}
                />
                <ErrorMessage
                  name="answer"
                  className="form-error"
                  component="div"
                />
              </div>
              <div className={classes.Btns}>
                <button
                  type="button"
                  name="submit"
                  className={`btn ${classes.BtnExecute}`}
                  disabled={isSubmitting}
                  onClick={() =>
                    handleExecute(values, {
                      setSubmitting,
                      setStatus,
                      validateForm,
                      setErrors,
                    })
                  }
                >
                  Execute
                </button>
                <button
                  type="submit"
                  name="submit"
                  className="btn"
                  disabled={isSubmitting}
                >
                  Submit
                </button>
              </div>
              {!!status?.serverError && (
                <Alert type={AlertType.DANGER}>{status.serverError}</Alert>
              )}
              {status?.error !== undefined && (
                <Alert
                  type={status.error ? AlertType.DANGER : AlertType.SUCCESS}
                >
                  <>
                    <strong>{!!status.error && "IN"}CORRECT ANSWER</strong>
                    <br />
                    <ul>
                      {!Array.isArray(status.msg)
                        ? status.msg
                        : status.msg.map(
                            (row: string | string[], i: number) => (
                              <li key={i}>
                                {!Array.isArray(row)
                                  ? row
                                  : row.map((col, j) => (
                                      <span key={j}> {col} |</span>
                                    ))}
                              </li>
                            )
                          )}
                    </ul>
                  </>
                </Alert>
              )}
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default Question;
