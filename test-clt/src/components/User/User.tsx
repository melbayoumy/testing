import React, { FC, useCallback } from "react";
import { ErrorMessage, Field, Form, Formik, FormikHelpers } from "formik";
import * as Yup from "yup";
import { getCurrentResult } from "../../lib/server/calls/users";
import { AxiosError } from "axios";
import Alert, { AlertType } from "../Alert/Alert";
import { Result } from "../../lib/server/calls/types";

interface Props {
  setEmail: (email: string) => void;
  setResult: (result: Result) => void;
}

interface Values {
  email: string;
}

const User: FC<Props> = ({ setEmail, setResult }) => {
  const validationSchema = Yup.object().shape({
    email: Yup.string().email("Invalid email").required("Required"),
  });

  const handleSubmit = useCallback(
    (values: Values, { setSubmitting, setStatus }: FormikHelpers<Values>) => {
      setSubmitting(true);
      setStatus({ error: undefined });

      let error = "Server error! please try again later";

      getCurrentResult(values.email)
        .then(({ data }) => {
          if (!data) {
            setStatus({ error });
            return;
          }

          setEmail(values.email);
          setResult(data);
        })
        .catch((e: AxiosError) => {
          if (e.response?.status === 400) {
            error = "Invalid email provided";
          }

          if (e.response?.status === 410) {
            error = "No more tests for this email";
          }

          setStatus({ error });
        })
        .finally(() => setSubmitting(false));
    },
    [setEmail, setResult]
  );

  return (
    <Formik
      initialValues={{ email: "" }}
      validationSchema={validationSchema}
      onSubmit={handleSubmit}
    >
      {({ isSubmitting, status }) => {
        return (
          <Form>
            {!!status?.error && (
              <Alert type={AlertType.DANGER}>{status.error}</Alert>
            )}
            <h4>Please enter you email</h4>
            <div className="form-group">
              <Field
                type="email"
                name="email"
                placeholder="email"
                className="form-field"
                disabled={isSubmitting}
              />
              <ErrorMessage
                name="email"
                className="form-error"
                component="div"
              />
            </div>
            <button type="submit" className="btn" disabled={isSubmitting}>
              Submit
            </button>
          </Form>
        );
      }}
    </Formik>
  );
};

export default User;
