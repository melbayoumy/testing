# TESTING 
This project contains 3 projects to serve as a Test Application -initally- for SQL assesment

## Databases
### Technology
* mysql 5.7

### Create locally
* Database server: `127.0.0.1:3306`

##### SQL-SRV DATABASE
* Access Mysql on your machine using root user e.g. for windows ``mysql -u<root> -p``
* Create database ``CREATE DATABASE `sql-srv`;``
* Create admin user ``CREATE USER 'sql-srv-rt' IDENTIFIED BY 'sql-srv-rt';``
* Assign privileges to user ``GRANT ALL PRIVILEGES ON `sql-srv`.* TO `sql-srv-rt`@`%`;``
* Create readonly user ``CREATE USER 'sql-srv-sb' IDENTIFIED BY 'sql-srv-sb';``
* Assign privileges to user ``GRANT SELECT ON `sql-srv`.`users` TO `sql-srv-sb`@`%`;``
* Assign privileges to user ``GRANT SELECT ON `sql-srv`.`google_users` TO `sql-srv-sb`@`%`;``

##### USERS-SRV DATABASE
* Access Mysql on your machine using root user e.g. for windows ``mysql -u<root> -p``
* Create database ``CREATE DATABASE `users-srv`;``
* Create admin user ``CREATE USER 'users-srv-rt' IDENTIFIED BY 'users-srv-rt';``
* Assign privileges to user ``GRANT ALL PRIVILEGES ON `users-srv`.* TO `users-srv-rt`@`%`;``

## SQL-SRV
SQL service provides the SQL questions and store the users' results for those questions

### Technology
* composer 2.0.14
* php >=7.2.5
* symfony 5.2

### Run locally
* Install dependencies ``php composer.phar install``
* Create database tables ``php ./bin/console doctrine:migrations:migrate``
* Import questions data. You need to manually import the `dump` in the `mysql_dump` dir to `sql-srv` database
* Spin local server ``php -S 127.0.0.1:8081 -t public``
* Now the service server is spinning on http://127.0.0.1:8081
* You can acces Open Api documentation on http://127.0.0.1:8081/doc/v1

## USERS-SRV
Users service provides the users data and connects with other service to get test results

### Technology
* composer 2.0.14
* php >=7.2.5
* symfony 5.2

### Run locally
* Install dependencies ``php composer.phar install``
* Create database tables ``php ./bin/console doctrine:migrations:migrate``
* Spin local server ``php -S 127.0.0.1:8080 -t public``
* Now the service server is spinning on http://127.0.0.1:8080
* You can acces Open Api documentation on http://127.0.0.1:8080/doc/v1
* To function properly, the users service needs `sql-srv` to be running

## TEST-CLT
Test client is the frontend web pages provider

### Technology
* Nodejs
* React 17
* Sass

### Run locally
* Install dependencies ``npm install``
* Spin local server ``npm run start``
* Now the client server is spinning on http://127.0.0.1:3000
* To function properly, the test client needs `users-srv` and `sql-srv` to be running