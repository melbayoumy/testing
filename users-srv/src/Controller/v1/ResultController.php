<?php

namespace App\Controller\v1;

use App\Entity\Test;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\SqlService;
use App\Validator\v1\UserValidator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use App\Service\ApiServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/results")
 * @OA\Tag(name="results")
 */
class ResultController extends AbstractController
{
    /** @var UserValidator */
    private $validator;

    /** @var UserRepository */
    private $repo;

    /** @var ApiServiceInterface[] */
    private $apiSrvs;

    public function __construct(
    UserValidator $validator, 
    UserRepository $repo,
    RewindableGenerator $apiSrvs   
    )
    {
        $this->validator = $validator;
        $this->repo = $repo;
        $this->apiSrvs = \iterator_to_array($apiSrvs);
    }

    /**
     * @Route("/email/{email}", methods={"GET"})
     * 
     * @OA\Get(summary="Get current question's result for provided email")
     * 
     * @OA\Response(
     *     response=200,
     *     description="Question's result",
     * )
     * 
     * @OA\Response(
     *     response=400,
     *     description="Bad parameters provided in the request body"
     * )
     * 
     * @OA\Response(
     *     response=410,
     *     description="No tests found for provided email"
     * )
     * 
     * @OA\Response(
     *     response=500,
     *     description="Api failure"
     * )
     */
    public function getCurrentQuestion($email): JsonResponse
    {
        if ($violations = $this->validator->validateEmail($email)) {
            return $this->json($violations, JsonResponse::HTTP_BAD_REQUEST);
        }
        
        $em = $this->getDoctrine()->getManager();
        
        try {
            $user = $this->repo->findOneWithTests($email);
            $result = null;

            if (!$user) {
                $test = Test::createSQLTest(1, true);

                $user = new User();
                $user->setEmail($email);
                $user->addTest($test);
                
                $em->persist($user);
                $em->flush();

                /** @var ApiServiceInterface */
                $service = $this->apiSrvs[SqlService::SRV_REF];
                $result = $service->addResult(1, $user->getId(), 1, false);
            }

            /** @var Test */
            $currentTest = null;

            /** @var Test */
            $nextTest = null;

            foreach($user->getTests() as $test) {
                if ($currentTest) {
                    $nextTest = $test;
                    break;
                }

                if ($test->getIsCurrent()) {
                    $currentTest = $test;
                }
            }

            if (!$currentTest) {
                return $this->json('No tests found for provided email!', JsonResponse::HTTP_GONE);
            }
            
            if (!$result) {
                /** @var ApiServiceInterface */
                $service = $this->apiSrvs[$currentTest->getRef()];
                $result = $service->getResult($user->getId());
            }

            if (!$result) {
                if ($nextTest) {
                    $this->setCurrentTest($user, $nextTest->getId());
                    $service = $this->apiSrvs[$nextTest->getRef()];
                    $result = $service->getResult($user->getId());
                } else {
                    $this->clearCurrentTest($user);
                    return $this->json('No test cases found for provided email!', JsonResponse::HTTP_GONE);
                }
            }

            return $this->json($result);
        } catch (\Exception $_) {
            return $this->json('Api failure!', JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Route("/email/{email}/next", methods={"GET"})
     * 
     * @OA\Get(summary="Get next question'result for provided email")
     * 
     * @OA\Response(
     *     response=200,
     *     description="Question's result",
     * )
     * 
     * @OA\Response(
     *     response=400,
     *     description="Bad parameters provided"
     * )
     * 
     * @OA\Response(
     *     response=410,
     *     description="No more tests found for provided email"
     * )
     * 
     * @OA\Response(
     *     response=500,
     *     description="Api failure"
     * )
     */
    public function getNextQuestion($email): JsonResponse
    {
        if ($violations = $this->validator->validateEmail($email)) {
            return $this->json($violations, JsonResponse::HTTP_BAD_REQUEST);
        }
        
        // call microservice
        try {
            $user = $this->repo->findOneWithTests($email);
            $result = null;

            if (!$user) {
                return $this->json('User not found!', JsonResponse::HTTP_NOT_FOUND);
            }

            /** @var Test */
            $currentTest = null;

            /** @var Test */
            $nextTest = null;

            foreach($user->getTests() as $test) {
                if ($test->getIsCurrent()) {
                    $currentTest = $test;
                }

                if ($currentTest) {
                    $nextTest = $test;
                    break;
                }
            }

            if (!$nextTest) {
                $this->clearCurrentTest($user);
                $this->json('No more tests found!', JsonResponse::HTTP_GONE);
            }

            $this->setCurrentTest($user, $nextTest->getId());

            /** @var ApiServiceInterface */
            $service = $this->apiSrvs[$nextTest->getRef()];
            $result = $service->getResult($user->getId());

            return $this->json($result);
        } catch (\Exception $_) {
            return $this->json('Api failure!', JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    
    private function setCurrentTest(User $user, int $testId) {
        foreach($user->getTests() as $test) {
            if ($test->getId() == $testId) {
                $test->setIsCurrent(true);
                continue;
            }

            $test->setIsCurrent(false);
        }

        $this->getDoctrine()->getManager()->flush();
    }

    private function clearCurrentTest(User $user) {
        foreach($user->getTests() as $test) {
            $test->setIsCurrent(false);
        }

        $this->getDoctrine()->getManager()->flush();
    }
}
