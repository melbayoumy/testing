<?php

namespace App\Entity;

use App\Repository\TestRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TestRepository::class)
 */
class Test
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $ref;

    /**
     * @ORM\Column(type="smallint")
     */
    private $queueNo;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="tests")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isCurrent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRef(): int
    {
        return $this->ref;
    }

    public function setRef(int $ref = 1): self
    {
        $this->ref = $ref;

        return $this;
    }

    public function getQueueNo(): ?int
    {
        return $this->queueNo;
    }

    public function setQueueNo(int $queueNo): self
    {
        $this->queueNo = $queueNo;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIsCurrent(): ?bool
    {
        return $this->isCurrent;
    }

    public function setIsCurrent(?bool $isCurrent): self
    {
        $this->isCurrent = $isCurrent;

        return $this;
    }

    // factory methods
    static public function createSQLTest(int $queueNo, bool $isCurrent): self 
    {
        $test = new self();
        $test->setRef(1);
        $test->setQueueNo($queueNo);
        
        if ($isCurrent) {
            if ($test->getUser()) {
                foreach($test->getUser()->getTests() as $t) {
                    $t->setIsCurrent(null);
                }
            }

            $test->setIsCurrent(true);
        } else {
            $test->setIsCurrent(null);
        }

        return $test;
    }
}
