<?php

namespace App\Validator;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationList;

abstract class BaseValidator
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param Constraint|Constraint[] $constraints
     * 
     * @return array<string, string>|null
     */
    protected function validate($data, $constraints): ?array
    {
        /** @var ConstraintViolationList */
        $violations = $this->validator->validate($data, $constraints);

        if (!\count($violations)) {
            return null;
        }

        $result = [];

        foreach($violations as $v) {
            $result[$v->getRoot()] = $v->getMessage();
        }

        return $result;
    }

    /**
     * @return array<string, string>|null
     */
    public function validateEmail(string $email): ?array
    {
        $violations = [];

        if ($v =  $this->validate($email, [
            new Assert\NotBlank(),
            new Assert\Email()
        ])) {
            $violations['email'] = \reset($v);
        }

        return $violations ?: null;  
    }
}
