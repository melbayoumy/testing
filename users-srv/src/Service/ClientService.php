<?php

namespace App\Service;

use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ClientService
{
    public const SRV_SQL = 1;

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getSrvClient(int $srvRef): ClientInterface
    {
        switch ($srvRef) {
            case self::SRV_SQL:
            default:
                return $this->container->get('csa_guzzle.client.sql_client');
        }
    }
}