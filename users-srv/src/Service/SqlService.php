<?php

namespace App\Service;

use App\Service\ClientService;
use GuzzleHttp\Psr7\Request;

class SqlService implements ApiServiceInterface
{
    public const SRV_REF = '1';

    private $client;

    public function __construct(ClientService $clientSrv)
    {
        $this->client = $clientSrv->getSrvClient(ClientService::SRV_SQL);
    }

    public static function getRef(): string
    {
        return self::SRV_REF;
    }

    public function getResult(int $userId)
    {
        $resp = $this->client->send(new Request('GET', "/v1/results/user/${userId}"));

        return json_decode($resp->getBody()->getContents(), true);
    }

    public function addResult(int $questionId, int $userId, int $queueNo, bool $start)
    {
        $start = $start ? 1 : 0;
        $resp = $this->client->send(new Request('POST', "/v1/results/question/${questionId}/user/${userId}/queueno/${queueNo}?start=${start}"));

        return json_decode($resp->getBody()->getContents(), true);
    }
}