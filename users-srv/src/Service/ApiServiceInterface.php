<?php

namespace App\Service;

interface ApiServiceInterface
{
    public static function getRef(): string;

    public function getResult(int $userId);

    public function addResult(int $id, int $userId, int $queueNo, bool $start);
}