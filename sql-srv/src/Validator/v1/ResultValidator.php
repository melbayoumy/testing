<?php

namespace App\Validator\v1;

use App\Validator\BaseValidator;
use Symfony\Component\Validator\Constraints as Assert;

class ResultValidator extends BaseValidator
{
    public function post($questionId, $userId, $queueNo)
    {
        $violations = [];

        if ($v = $this->validate($questionId, new Assert\GreaterThan(0))) {
            $violations['questionId'] = \reset($v);
        }

        if ($v = $this->validate($userId, new Assert\GreaterThan(0))) {
            $violations['userId'] = \reset($v);
        }

        if ($v = $this->validate($queueNo, new Assert\GreaterThan(0))) {
            $violations['queueNo'] = \reset($v);
        }

        return $violations ?: null;
    }

    public function postMany($userId, $data) 
    {
        $violations = [];

        if ($v = $this->validate($userId, new Assert\GreaterThan(0))) {
            $violations['userId'] = \reset($v);
        }

        $constraints = [
            'questionId' => new Assert\GreaterThan(0),
            'queueNo' => new Assert\GreaterThan(0),
        ];

        if ($v = $this->validate($data, $constraints)) {
            $violations[] = \array_merge($violations, $v);
        }

        return $violations ?: null;
    }

    public function checkAnswer($questionId, $answer) 
    {
        $violations = [];

        if ($v = $this->validate($questionId, new Assert\GreaterThan(0))) {
            $violations['questionId'] = \reset($v);
        }

        if ($v = $this->validate($answer, new Assert\NotBlank())) {
            $violations['answer'] = \reset($v);
        }

        return $violations ?: null;
    }

    public function submitAnswer($resultId, $answer)
    {
        $violations = [];

        if ($v = $this->validate($resultId, new Assert\GreaterThan(0))) {
            $violations['resultId'] = \reset($v);
        }

        if ($v = $this->validate($answer, new Assert\NotBlank())) {
            $violations['answer'] = \reset($v);
        }

        return $violations ?: null;   
    }

    public function startAnswering($resultId)
    {
        $violations = [];

        if ($v = $this->validate($resultId, new Assert\GreaterThan(0))) {
            $violations['resultId'] = \reset($v);
        }

        return $violations ?: null;   
    }
}
