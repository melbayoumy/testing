<?php

namespace App\Validator;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationList;

abstract class BaseValidator
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param Constraint|Constraint[] $constraints
     * 
     * @return array<string, string>|null
     */
    public function validate($data, $constraints): ?array
    {
        /** @var ConstraintViolationList */
        $violations = $this->validator->validate($data, $constraints);

        if (!\count($violations)) {
            return null;
        }

        $result = [];

        foreach($violations as $v) {
            $result[$v->getRoot()] = $v->getMessage();
        }

        return $result;
    }
}
