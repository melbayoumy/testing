<?php

namespace App\Repository;

use App\Entity\Result;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Result|null find($id, $lockMode = null, $lockVersion = null)
 * @method Result|null findOneBy(array $criteria, array $orderBy = null)
 * @method Result[]    findAll()
 * @method Result[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Result::class);
    }

    public function findCurrentUnAnsweredByUserId(int $userId): ?Result
    {
        return $this->createQueryBuilder('r')
            ->addSelect('q')
            ->leftJoin('r.question', 'q')
            ->andWhere('r.userId = :userId')
            ->andWhere('r.finishedOn IS NULL OR r.startedOn IS NULL')
            ->setParameter('userId', $userId)
            ->orderBy('r.queueNo', 'ASC')
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findWithQuestion(int $id): ?Result
    {
        return $this->createQueryBuilder('r')
            ->addSelect('q')
            ->leftJoin('r.question', 'q')
            ->andWhere('r.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
