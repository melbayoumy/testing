<?php

namespace App\Controller\v1;

use App\Repository\ResultRepository;
use App\Repository\QuestionRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use App\Entity\Result;
use App\Validator\v1\ResultValidator;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Route("/results")
 * @OA\Tag(name="results")
 */
class ResultController extends AbstractController
{
    /** @var ResultRepository */
    private $resultRepo;

    /** @var QuestionRepository */
    private $questionRepo;

    /** @var ResultValidator */
    private $validator;

    public function __construct(
        ResultRepository $resultRepo,
        QuestionRepository $questionRepo,
        ResultValidator $validator
    )
    {
        $this->resultRepo = $resultRepo;
        $this->questionRepo = $questionRepo;
        $this->validator = $validator;
    }

    /**
     * @Route("/user/{userId}", methods={"GET"})
     * 
     * @OA\Get(summary="Get current question result for the provided user email")
     * 
     * @OA\Response(
     *     response=200,
     *     description="Result with its question",
     *     @Model(type=Result::class)
     * )
     * 
     * @OA\Response(
     *     response=204,
     *     description="No unanswered question found"
     * )
     * 
     * @OA\Response(
     *     response=400,
     *     description="Bad request parameters"
     * )
     */
    public function getCurrentByUserId($userId): JsonResponse
    {   
        if ($violations = $this->validator->validate($userId, new Assert\GreaterThan(0))) {
            return $this->json($violations, JsonResponse::HTTP_BAD_REQUEST);
        }

        $result = $this->resultRepo->findCurrentUnAnsweredByUserId($userId);

        if ($result && !$result->getStartedOn()) {
            $result->setStartedOn(new \DateTimeImmutable());
            $this->getDoctrine()->getManager()->flush();
        }

        if ($result) {
            return $this->json($result);
        }

        return $this->json('', JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/question/{questionId}/user/{userId}/queueno/{queueNo}", methods={"POST"})
     * 
     * @OA\Post(summary="Link question for certain user")
     * 
     * @OA\Response(
     *     response=200,
     *     description="Result with its question",
     *     @Model(type=Result::class)
     * )
     * 
     * @OA\Response(
     *     response=400,
     *     description="Bad request parameters or Question not found"
     * )
     * 
     * @OA\Parameter(
     *     name="start",
     *     in="query",
     *     description="The question should be started to be answered or not"
     * )
     * 
     */
    public function post($questionId, $userId, $queueNo, Request $request): JsonResponse
    {
        if ($violations = $this->validator->post($questionId, $userId, $queueNo)) {
            return $this->json($violations, JsonResponse::HTTP_BAD_REQUEST);
        }

        $start = $request->query->getBoolean('start');

        $question = $this->questionRepo->find($questionId);

        if (!$question) {
            return $this->json("Question not found!", JsonResponse::HTTP_BAD_REQUEST);
        }

        $result = new Result();
        $result->setQuestion($question);
        $result->setUserId($userId);
        $result->setQueueNo($queueNo);

        if ($start) {
            $result->setStartedOn(new \DateTimeImmutable());
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($result);
        $em->flush();

        return $this->json($result);
    }

    /**
     * @Route("/user/{userId}", methods={"POST"})
     * 
     * @OA\Post(summary="Link many questions for certain user")
     * 
     * @OA\RequestBody(
     *      required=true,
     *      @OA\JsonContent(
     *          type="array",
     *          items=@OA\Items(
     *              @OA\Property(property="questionId", type="integer", example=1),
     *              @OA\Property(property="queueNo", type="integer", example=1),
     *              @OA\Property(property="start", type="boolean")
     *          )
     *     )
     * )
     * 
     * @OA\Response(
     *     response=200,
     *     description="Output messages and current question's result",
     *     @OA\JsonContent(
     *         @OA\Property(property="msg", type="array", items=@OA\Property(type="string"), example={"Question of id 3 not found!","Question of id 5 was assigned to user successfully"}),
     *         @OA\Property(property="current", ref=@Model(type=Result::class))
     *     )
     * )
     * 
     * @OA\Response(
     *     response=400,
     *     description="Bad request parameters"
     * )
     */
    public function postMany($userId, Request $request): JsonResponse
    {
        $data = \json_decode($request->getContent(), true);

        if ($violations = $this->validator->postMany($userId, $data)) {
            return $this->json($violations, JsonResponse::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();
        $msg = [];
        /** @var Result */
        $currentResult = null;

        foreach($data as $item) {
            $questionId = $item['questionId'];
            $queueNo = $item['queueNo'];
            $start = empty($item['start']) ? false : true;

            $question = $this->questionRepo->find($questionId);

            if (!$question) {
                $msg[] = "Question of id $questionId not found!";
                continue;
            }

            $result = new Result();
            $result->setQuestion($question);
            $result->setUserId($userId);
            $result->setQueueNo($queueNo);
            if ($start) {
                $result->setStartedOn(new \DateTimeImmutable());
            }

            $em->persist($result);

            if (!$currentResult || $currentResult->getQueueNo() > $queueNo) {
                $currentResult = $result;
            }

            $msg[] = "Question of id $questionId was assigned to user successfully";
        }

        $em->flush();

        return $this->json([
            'msg' => $msg,
            'current' => $currentResult
        ]);
    }

    /**
     * @Route("/question/{questionId}/check", methods={"POST"})
     * 
     * @OA\Post(summary="Check if question answer is correct")
     * 
     * @OA\RequestBody(
     *      required=true,
     *      description="answer",
     *      @OA\JsonContent(
     *          type="string",
     *     )
     * )
     * 
     * @OA\Response(
     *     response=200,
     *     description="Submitted answer is true/false or returns error message",
     *     @OA\JsonContent(
     *         @OA\Property(property="correct", type="boolean"),
     *         @OA\Property(property="msg", type="string", example=""),              
     *         examples={
     *              @OA\Examples(example="Correct answer", summary="Correct answer", value="{correct: true, msg: ''}"),
     *              @OA\Examples(example="Incorrect answer", summary="Incorrect answer", value="{correct: false, msg: '[{},{}]'}"),
     *              @OA\Examples(example="Incorrect answer - exception", summary="Incorrect answer - exception", value="{correct: false, msg: 'SQLSTATE[42S02]: Base table or view not found: 1146 Table 'sql-srv.google_user' doesn't exist'}")
     *         },
     *     ),
     * )
     * 
     * @OA\Response(
     *     response=400,
     *     description="Bad request parameters"
     * )
     * 
     * @OA\Response(
     *     response=404,
     *     description="Question not found"
     * )
     */
    public function checkAnswer($questionId, Request $request): JsonResponse
    {
        $sql = \json_decode($request->getContent(), true);

        if ($violations = $this->validator->checkAnswer($questionId, $sql)) {
            return $this->json($violations, JsonResponse::HTTP_BAD_REQUEST);
        }

        $question = $this->questionRepo->find($questionId);

        if (!$question) {
            return $this->json("Question not found!", JsonResponse::HTTP_NOT_FOUND);
        }

        $answer = $question->getAnswer();

        $output = ['correct' => false, 'msg' => ''];
        try {
            $output = $this->validateAnswer($sql, $answer);
        } catch (\Exception $e) {
            $output['msg'] = $e->getMessage();
        }

        return $this->json($output);
    }

    /**
     * @Route("/{resultId}", methods={"POST"})
     * 
     * @OA\Post(summary="Submit question answer and return next question")
     * 
     * @OA\RequestBody(
     *      required=true,
     *      description="answer",
     *      @OA\JsonContent(
     *          type="string",
     *     )
     * )
     * 
     * @OA\Response(
     *     response=200,
     *     description="Next question",
     *     @Model(type=Result::class)
     * )
     * 
     * @OA\Response(
     *     response=204,
     *     description="No more questions"
     * )
     * 
     * @OA\Response(
     *     response=400,
     *     description="Bad request data"
     * )
     * 
     * @OA\Response(
     *     response=410,
     *     description="Question already answered"
     * )
     * 
     * @OA\Response(
     *     response=404,
     *     description="Result not found"
     * )
     */
     public function submitAnswer($resultId, Request $request): JsonResponse
     {
 
         $sql = \json_decode($request->getContent(), true);
 
         if ($violations = $this->validator->submitAnswer($resultId, $sql)) {
             return $this->json($violations, JsonResponse::HTTP_BAD_REQUEST);
         }
 
         $result = $this->resultRepo->findWithQuestion($resultId);
 
         if (!$result) {
             return $this->json("Result not found!", JsonResponse::HTTP_NOT_FOUND);
         }
 
         if ($result->getFinishedOn()) {
             return $this->json("Question already answered!", JsonResponse::HTTP_GONE);
         }
 
         $answer = $result->getQuestion()->getAnswer();
 
         try {
             $isCorrect = $this->validateAnswer($sql, $answer)['correct'];
         } catch(\Exception $_) {
             $isCorrect = false;
         }
 
         $result->setIsCorrect($isCorrect);
         $result->setFinishedOn(new \DateTimeImmutable());
 
         $em = $this->getDoctrine()->getManager();
 
         $em->flush();
 
         // get next question
         $next = $this->resultRepo->findCurrentUnAnsweredByUserId($result->getUserId());
 
         if (!$next) {
             return $this->json('', JsonResponse::HTTP_NO_CONTENT);
         }
 
         return $this->json($next);
     }
     
    /**
     * @Route("/{resultId}/start", methods={"PATCH"})
     * 
     * @OA\Patch(summary="Start answering question's result")
     * 
     * @OA\Response(
     *     response=200,
     *     description="question result",
     *     @Model(type=Result::class)
     * )
     * 
     * @OA\Response(
     *     response=400,
     *     description="Bad request data"
     * )
     * 
     * @OA\Response(
     *     response=410,
     *     description="Question result already started"
     * )
     * 
     * @OA\Response(
     *     response=404,
     *     description="Result not found"
     * )
     */
    public function startAnswering($resultId, Request $request): JsonResponse
    {

        $sql = \json_decode($request->getContent(), true);

        if ($violations = $this->validator->startAnswering($resultId)) {
            return $this->json($violations, JsonResponse::HTTP_BAD_REQUEST);
        }

        $result = $this->resultRepo->findWithQuestion($resultId);

        if (!$result) {
            return $this->json("Result not found!", JsonResponse::HTTP_NOT_FOUND);
        }

        if ($result->getStartedOn()) {
            return $this->json("Question result already started!", JsonResponse::HTTP_GONE);
        }

        $result->setStartedOn(new \DateTimeImmutable());

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return $this->json($result);
    }

    private function validateAnswer(string $query, string $answerExpected): array
    {
        /** @var Connection */
        $conn = $this->getDoctrine()->getConnection('submissions');

        $stmt = $conn->prepare($query);
        $result = $stmt->executeQuery();

        $answer = $result->fetchAllAssociative();

        $correct = true;
        $msg = [];
        foreach(\unserialize($answerExpected) as $i => $rowExpected) {
            $row = \array_values($answer[$i]);
            $msg[] = $row;

            foreach($rowExpected as $j => $colExpected) {
                if ($colExpected != $row[$j]) {
                    $correct = false;
                }
            }
        } 

        return ['correct' => $correct, 'msg' => $msg];
    }
}
