<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210527172344 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Import question 1';
    }

    public function up(Schema $schema): void
    {
        $answer = \serialize([
            ['Desktop', '99'],
            ['Mobile', '43'],
            ['Tablet', '16'],
        ]);

        $this->addSql('INSERT INTO question (id, score, text, answer) VALUES ' . 
        '(1, 10, ' .
        '\'<article> <strong>users</strong><table><tbody><tr><td>user_id</td><td>int not null</td></tr><tr><td>is_activated</td><td>boolean not null</td></tr><tr><td>signed_up_on</td><td>datetime not null</td></tr><tr><td>last_login</td><td>datetime nullable</td></tr><tr><td>sign_up_source</td><td>string nullable</td></tr><tr><td>unsubscribed</td><td>tinyint not null</td></tr><tr><td>user_type</td><td>tinyint not null</td></tr></tbody></table> <strong>google_users</strong><table><tbody><tr><td>id</td><td>int not null</td></tr><tr><td>user_id</td><td>int not null</td></tr><tr><td>browser_language_code</td><td>string not null</td></tr><tr><td>created_on</td><td>datetime nullable</td></tr><tr><td>device_cat</td><td>string nullable</td></tr></tbody></table> </article><article style="padding-bottom: 20px;"><p style="padding-bottom: 10px;"> Write a query to get the number of unique Google users whose last login was in July, 2019, broken down by device type. Show the most used device in that period first.</p> <strong>Expected Output:</strong> </br> Desktop | 99</br> Mobile | 43</br> Tablet | 16</article>\', '.'
        \'' . $answer . '\')');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('SET FOREIGN_KEY_CHECKS=0');
        $this->addSql('DELETE FROM question WHERE id=1');
        $this->addSql('SET FOREIGN_KEY_CHECKS=1');
    }
}
